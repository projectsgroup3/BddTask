Feature: Check product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

### Positive scenario with valid product name, product type
  @positive
  Scenario Outline:  Search products with valid data
    When  admin calls product list endpoint
    And "positive" case user calls endpoint to get product "<productType>" with product name "<productName>" with status code <statusCode>
    Then user sees the results displayed for products

    Examples:
      | productType | productName | statusCode |
      | apple       | apple       | 200        |
      | orange      | orange      | 200        |
      | pasta       | pasta       | 200        |
      | cola        | cola        | 200        |




### Negative scenario with invalid product name, product type
  @negative
  Scenario Outline: Search products  with invalid data
    When  admin calls product list endpoint
    And "negative" case user calls endpoint to get product "<productType>" with product name "<productName>" with status code <statusCode>
    Then user sees the results displayed for products

    Examples:
      | productType | productName | statusCode |
      | apple       | house       | 200        |
      | house       | apple       | 404        |



