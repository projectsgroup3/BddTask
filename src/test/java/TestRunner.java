/**
 * @author Arthur Darbinyan on 6/27/23.
 */

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/feature.files",
        tags = "@positive or @negative"
)
public class TestRunner {
}
