/**
 * @author Arthur Darbinyan on 6/27/23.
 */

package stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import pageobjects.ProductsAPI;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;


public class SearchStepDefinitions {
    SoftAssertions softly = new SoftAssertions();

    @Steps
    ProductsAPI productsAPI;


    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }


    @When("^(.*) calls product list endpoint")
    public void user_calls_product_list_endpoint(String actor) {
        theActorCalled(actor).can(CallAnApi.at(productsAPI.getBaseUrl()));
    }


    @And("{string} case user calls endpoint to get product {string} with product name {string} with status code {int}")
    public void user_calls_endpoint_to_get_product(String scenario, String product, String productName, int statusCode) {
        switch (scenario) {
            case "positive" -> {
                softly.assertThat(productsAPI.getProduct(product).get(0).contains(productName))
                        .withFailMessage("looking for " + product + " failed ")
                        .isTrue();
                softly.assertThat(productsAPI.getProduct(product).get(1).contains(String.valueOf(statusCode)))
                        .withFailMessage("Status code should be" + statusCode)
                        .isTrue();
            }
            case "negative" -> {
                softly.assertThat(productsAPI.getProduct(product).get(0).contains(productName))
                        .withFailMessage("looking for " + product + " failed ")
                        .isFalse();
                softly.assertThat(productsAPI.getProduct(product).get(1).contains(String.valueOf(statusCode)))
                        .withFailMessage("Status code should be" + statusCode)
                        .isTrue();
            }
            default -> throw new IllegalStateException("Unexpected host: Check BaseUrl at serenity.properties ");
        }

    }


    @Then("user sees the results displayed for products")
    public void userChecksTheResults() {
        softly.assertAll();
    }

}
