/**
 * @author Arthur Darbinyan on 6/27/23.
 */


package pageobjects;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;

import java.util.ArrayList;
import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class ProductsAPI {
    public EnvironmentVariables environmentVariables;


    @Step
    public String getBaseUrl() {
        return environmentVariables.getProperty("BaseUrl");
    }

    @Step
    public List<String> getProduct(String productName) {
        theActorInTheSpotlight().attemptsTo(Get.resource(productName));
        List<String> returnValues = new ArrayList<>();
        returnValues.add(SerenityRest.lastResponse().getBody().asString());
        returnValues.add(String.valueOf(SerenityRest.lastResponse().statusCode()));
        return returnValues;
    }
}
