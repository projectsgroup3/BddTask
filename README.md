# BddTask



## Getting started

To clone the project or run pipelines on gitlab.


- [ ] [Project Url](https://gitlab.com/projectsgroup3/BddTask/)




## Build and Test
Login to Gitlab with verified account (if account is not verified user own runners)

- [ ] [To build and test the BddTask project use CI/CD pipeline](https://gitlab.com/projectsgroup3/BddTask/-/pipelines/new
  )


- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)

***

# Verify test run by test reports

Open Job details and download or browse job artifacts, where you can see serenity_summary_report file
- NOTE: Files are active only 5 days  after run.

-[ ] [Artifacts location Example](https://gitlab.com/projectsgroup3/BddTask/-/jobs/4551123186)

***

# Run from TestRunner

* Run src/test/java/TestRunner.java where is defined scenario tags which can be changed based on test purpose, 
whether user wants to check positive or negative cases


# What was changed

* Changed Assertion types to soft Assertions to avoid from skipped tests.
* Base Url moved to serenity.properties file.
* Added 2 positive and negative scenario outlines.
* Added serenity summary report.
* Updated versions of dependencies in pom.xml to compile and run with JDK 19.0.2.
* Removed all unnecessary files and dependencies which were not used.


# Author
  With Best Regards Arthur Darbinyan


